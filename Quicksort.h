#ifndef QUICKSORT_H
#define QUICKSORT_H

class Quicksort{
    private:
        //Tamaño de la lista
        int n;
    public:
        //Se instancian las funciones
        Quicksort();
        Quicksort(int n);
        void implementar_orden(int* lista);
        double ordenar(int* lista);
};
#endif