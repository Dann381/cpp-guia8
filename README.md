# Guía 8 - Unidad III

### Descripción
Este algoritmo consiste en la implementación de dos métodos de ordenamiento, llamados "Selection" y "Quicksort", los cuales serán comparados según el tiempo que tarda cada uno en ordenar una misma lista de números aleatorios de tamaño a elección del usuario.  

### Ejecución
Luego de descargar los archivos contenidos en este repositorio, el usuario debe escribir vía terminal el comando "make" para compilar los archivos. Esto dejará un ejecutable llamado "programa", el cual recibe un parámetro inicial de tipo entero luego de escribir el nombre del programa, y un segundo parámetro para mostrar los datos de cada lista creada durante la ejecución del algoritmo (ejemplo: ./programa 100 s). El comando anterior iniciará el programa con una lista de 100 números aleatorios, además de mostrar los datos de la lista antes y después de ordenar.
- Primer parámetro: Es un número entero que define la cantidad de números que tendrá la lista a crear.
- Segundo parámetro: Es una parámetro de tipo string el cual puede ser "s" o "S" para mostrar los valores de cada lista además de los tiempos que tardó cada método, o de lo contrario "n" o "N" para solo mostrar los tiempos de cada método.
- Si el usuario no ingresa un segundo parámetro, el programa por defecto solo mostrará los tiempo de cada método (sin los datos las listas).
- Ejemplos a probar: **./programa 10 s** o **./programa 20000 n**


### Luego de ejecutar
Si se ha ejecutado el programa correctamente, el programa mostrará por la terminal el contenido de la lista inicial de tamaño a pedido del usuario con números aleatorios, luego mostrará el tiempo que tardó cada método en ordenar cada lista, y por último mostrará las listas ordenadas por cada método.

### Construido con
El presente proyecto de programación, se construyó en base al lenguaje de programación C++, en conjunto con el IDE "Visual Studio Code".

* [C++] - Lenguaje de programación utilizado.
* [Visual Studio Code](https://code.visualstudio.com/) - IDE utilizado.

### Autores
* **Daniel Tobar** - Estudiante de Ingeniería Civil en Bioinformática.
