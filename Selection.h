#ifndef SELECTION_H
#define SELECTION_H

class Selection{
    private:
        //Tamaño de la lista
        int n;
    public:
        //Se instancian las funciones
        Selection();
        Selection(int n);
        void implementar_orden(int* lista);
        double ordenar(int* lista);
};
#endif