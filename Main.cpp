#include <time.h>
#include <iostream>
using namespace std;
#include "Selection.h"
#include "Quicksort.h"

int main(int argc, char **argv){
    system("clear");
    srand(time(NULL));

    //Recibe los argumentos pasados desde la terminal
    int n = atoi(argv[1]); //Tamaño de la lista
    string ver = "a";
    if(argv[2]){
        ver = argv[2]; //Opción para ver los datos de la lista
    }
    //Se crean dos listas iguales, una se ordenará con el método Selection y la otra con Quicksort
    int *lista;
    int *lista1;
    lista = new int[n];
    lista1 = new int[n];

    //Se llenan las listas con los mismos números aleatorios (entre 1 y 5000)
    int temp;
    for (int i=0; i<n; i++){
        temp = (rand() % 5000) + 1;
        lista[i] = temp;
        lista1[i] = temp;
    }

    //Si el usuario ingresa n o N como segundo parámetro, se le dirá que solo se mostrarán los tiempos de cada algoritmo
    if (ver == "n" || ver == "N"){
        cout << "Mostrando los tiempos de cada algoritmo" << endl;
    }
    //Si el usuario ingresa n o N como segundo parámetro, se mostrará el contenido de las listas además de los tiempos
    if (ver == "s" || ver == "S"){
        cout << "Mostrando los datos de la listas y los tiempos de cada algoritmo" << endl;
        cout << "Lista original: ";
        for (int i=0; i<n; i++){
            cout << lista[i] << " | ";
        }
        cout << "\n" << endl;
    }
    //Si el usuario no ingresa ningún parametro despues del tamaño de la lista, se mostrarán solo los tiempos
    //por defecto (sin los datos de las listas)

    //Se crea un objeto de tipo Selection con las funciones para implementar este algoritmo    
    Selection selection = Selection(n); //Se crea el objeto con la variable "n" que corresponde al tamaño de la lista
    double selection_time = selection.ordenar(lista); //Se le pasa la lista a ordenar

    //Se crea un objeto de tipo Quicksort con las funciones para implementar este algoritmo
    Quicksort quicksort = Quicksort(n); //Se crea el objeto con la variable "n" que corresponde al tamaño de la lista
    double quicksort_time = quicksort.ordenar(lista1); //Se le pasa la lista a ordenar
    
    //A continuación se muestra la comparación entre los tiempos de cada algoritmo (en milisegundos y segundos)
    cout << "<<<<< Comparación tiempos Selection y Quicksort >>>>>" << endl;
    cout << "------------------------------------------------------------" << endl;
    cout << "Método       | Tiempo         " << endl;
    cout << "Selection    | Milisegundos: " << selection_time*1000 << " >> Segundos: " << selection_time << endl;
    cout << "Quicksort    | Milisegundos: " << quicksort_time*1000 << " >> Segundos: " << quicksort_time << endl;
    cout << "------------------------------------------------------------" << endl;

    //Si el usuario ingresó una s o S como segundo parámetro, se mostrará el contenido de las listas despues de ordenar
    if (ver== "s" || ver == "S"){
        cout << "\nLista Selection: ";
        for (int i=0; i<n; i++){
            cout << lista[i] << " | ";
        }
        cout << "\n" << endl;
        cout << "Lista Quicksort: ";
        for (int i=0; i<n; i++){
            cout << lista1[i] << " | ";
        }
        cout << endl;

    }
    return 0;
}
