#include <time.h>
#include <iostream>
using namespace std;
#include "Selection.h"

//La variable n corresponde al tamaño de la lista
Selection::Selection(){
    int n;
}

//Creación de la clase
Selection::Selection(int n){
    this->n = n;
}

//Implementación del algoritmo Selection
void Selection::implementar_orden(int* lista){
    //Variables necesarias. Menor guarda el valor de la posición actual, y K sirve para cambiar de posición un valor
    int menor, k;
    //Se entra al ciclo hasta (n-2)
    for (int i=0; i<=(n-2); i++) {
        //Menor adquiere el valor de la posición actual
        menor = lista[i];
        //K adquiere la posición actual
        k = i;
        //Se entra a un segundo ciclo desde i+1 hasta el final de la lista (n-1)
        for (int j=(i+1); j<=(n-1); j++) {
            //Si el valor siguiente a la posición actual es menor, se intercambian los valores
            if (lista[j] < menor) {
                menor = lista[j];
                k = j;
            }
        }
        //Se actualizan los valores
        lista[k] = lista[i];
        lista[i] = menor;
    }
}

//Función que funciona como "main" de este algoritmo
double Selection::ordenar(int* lista){
    //Objetos de tipo clock_t para guardar el tiempo
    clock_t t1, t2;
    double secs;
    //Se toma el timepo antes de implementar el algoritmo
    t1 = clock();
    //Se implementa el algoritmo
    implementar_orden(lista);
    //Se toma el tiempo al finalizar la implementación
    t2 = clock();
    //Se calculan los milisegundos que domoró el algoritmo en termina
    secs = (double)(t2 - t1) / CLOCKS_PER_SEC;
    return secs;
}