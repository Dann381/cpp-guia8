#include <time.h>
#include <iostream>
using namespace std;
#include "Quicksort.h"

//La variable n corresponde al tamaño de la lista
Quicksort::Quicksort(){
    int n;
}

//Creación de la clase
Quicksort::Quicksort(int n){
    this->n = n;
}

//Implementación del algoritmo Quicksort
void Quicksort::implementar_orden(int* lista){
    //Variables para controlar posiciones en la lista
    int tope, ini, fin, pos;
    int *pilamenor;
    int *pilamayor;
    //Pilas para controlar el ciclo while donde se ordenará la lista
    pilamenor = new int[100];
    pilamayor = new int[100];
    //Variables para cambiar de posición las variables, y un booleano para controlar un ciclo interior
    int izq, der, aux, band;
    tope = 0;
    pilamenor[tope] = 0;
    pilamayor[tope] = n-1;
    
    //Se inicia el ciclo que se detiene según el valor de la variable tope
    while (tope >= 0) {
        ini = pilamenor[tope];
        fin = pilamayor[tope];
        tope = tope - 1;
    
        //Se actualizan las variables
        izq = ini;
        der = fin;
        pos = ini;
        band = true;

        //Si band es verdadero, se entra al ciclo
        while (band == true) {
            //Compara el valor actual con los valores de su derecha
            while ((lista[pos] <= lista[der]) && (pos != der)){
                der = der - 1;
            }

            //Si "der" llega a ser igual a "pos", significa que no existen valores menores a lista[pos] por la derecha
            if (pos == der) {
                //Booleano para no volver al ciclo
                band = false;
            }
            //Si hay algún valor a lista[pos] por la derecha, se intercambia el valor de lista[pos] por ese valor
            else {
                aux = lista[pos];
                lista[pos] = lista[der];
                lista[der] = aux;
                pos = der;

                //Compara los valores de lista[pos] con los valores de la izquierda, y repite lo anterior pero con esos valores
                while ((lista[pos] >= lista[izq]) && (pos != izq)){
                    izq = izq + 1;
                }
          
                if (pos == izq) {
                    band = false;
                } 
                else {
                    aux = lista[pos];
                    lista[pos] = lista[izq];
                    lista[izq] = aux;
                    pos = izq;
                }
            }
        }
    
        //Con los siguientes ifs se controla el valor "tope" para volver a implementar Quicksort
        if (ini < (pos - 1)) {
            tope = tope + 1;
            pilamenor[tope] = ini;
            pilamayor[tope] = pos - 1;
        }
    
        if (fin > (pos + 1)) {
            tope = tope + 1;
            pilamenor[tope] = pos + 1;
            pilamayor[tope] = fin;
        }
    }
}

//Función que funciona como "main" de este algoritmo
double Quicksort::ordenar(int* lista){
    //Objetos de tipo clock_t para guardar el tiempo
    clock_t t1, t2;
    double secs;
    //Se toma el timepo antes de implementar el algoritmo
    t1 = clock();
    //Se implementa el algoritmo
    implementar_orden(lista);
    //Se toma el tiempo al finalizar la implementación
    t2 = clock();
    //Se calculan los milisegundos que domoró el algoritmo en terminar
    secs = (double)(t2 - t1) / CLOCKS_PER_SEC;
    return secs;
}